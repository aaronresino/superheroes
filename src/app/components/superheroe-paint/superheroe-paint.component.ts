import { Component, effect, input } from '@angular/core';

import { IImage } from '../../models/image.interface';
import { Option } from '../../utils/constants';

@Component({
  selector: 'app-superheroe-paint',
  standalone: true,
  templateUrl: './superheroe-paint.component.html',
  styleUrl: './superheroe-paint.component.css'
})
export class SuperheroePaintComponent {
  public imageToPaint = input<IImage>();
  public urlHead?: string;
  public urlBody?: string;
  public urlLegs?: string;
  public urlCape?: string;

  constructor() {
    effect(() => {
      switch (this.imageToPaint()?.type) {
        case Option.Faces:
          this.urlHead = this.imageToPaint()?.url;
          break;

        case Option.Body:
          this.urlBody = this.imageToPaint()?.url;
          break;

        case Option.Legs:
          this.urlLegs = this.imageToPaint()?.url;
          break;

        case Option.Cape:
          this.urlCape = this.imageToPaint()?.url;
          break;
      }
    });
  }
}
