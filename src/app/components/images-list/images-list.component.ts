import {
  Component,
  EventEmitter,
  Output,
  effect,
  inject,
  input,
} from '@angular/core';

import { ImagesService } from '../../services/images.service';
import { IImage } from '../../models/image.interface';
import { Option } from '../../utils/constants'

@Component({
  selector: 'app-images-list',
  standalone: true,
  providers: [ImagesService],
  templateUrl: './images-list.component.html',
  styleUrl: './images-list.component.css',
})
export class ImagesListComponent {
  public type = input.required<Option>();
  public imageList: IImage[] = [];
  public readonly Option = Option;

  private imagesService: ImagesService = inject(ImagesService);

  @Output()
  public elementToShow = new EventEmitter<IImage>();

  constructor() {
    effect(() => this.getImagesListByType(this.type()));
  }

  private getImagesListByType(type: Option): void {
    this.imageList = this.imagesService.getImagesByName(type);
  }

  public imageToPaint(image: IImage): void {
    this.elementToShow.emit(image);
  }
}
