import { Component } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

import { Option } from './utils/constants';
import { ImagesListComponent } from './components/images-list/images-list.component';
import { IImage } from './models/image.interface'
import { SuperheroePaintComponent } from './components/superheroe-paint/superheroe-paint.component'

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  imports: [ImagesListComponent, SuperheroePaintComponent, TitleCasePipe],
})
export class AppComponent {
  public selectedOption = Option.Faces;
  public optionList = [
    Option.Faces,
    Option.Body,
    Option.Legs,
    Option.Cape
  ];
  public imageToPaint?: IImage;

  getImageToPaint(image: IImage): void {
    this.imageToPaint = image;
  }
}
