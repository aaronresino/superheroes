import { Injectable } from '@angular/core';

import * as data from '../data/data.json';
import { IImage } from '../models/image.interface'

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  /**
   * Returns name and image url of the image.
   * @param name
   */
  public getImagesByName(name: string): IImage[] {
    return data.imagesList.find(image => image.name === name)!.images;
  }
}
