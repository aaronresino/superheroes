import { IImage } from './image.interface'

export interface IPart {
  name: string;
  images: IImage[];
}
