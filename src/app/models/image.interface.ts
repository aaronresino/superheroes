export interface IImage {
  name: string;
  type: string;
  url: string;
}
